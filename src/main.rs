use std::str::FromStr;

use clap::{Parser, Subcommand};
use dialoguer::theme::ColorfulTheme;
use dialoguer::Password;
use futures::stream::StreamExt;
use tokio;
use tokio_xmpp::SimpleClient as Client;
use uuid::Uuid;
use xmpp_parsers::data_forms::{DataForm, Field};
use xmpp_parsers::disco::DiscoInfoQuery;
use xmpp_parsers::iq::Iq;
use xmpp_parsers::pubsub::owner::Delete;
use xmpp_parsers::pubsub::pubsub::{
    Configure, Create, Item, Items, Notify, Publish, PublishOptions, Retract,
};
use xmpp_parsers::pubsub::{Item as PubSubItem, ItemId, NodeName, PubSub, PubSubOwner};
use xmpp_parsers::{Element, Jid};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct AriadneXmppCommand {
    /// The Jabber ID to login with
    #[clap(long)]
    jid: Jid,

    /// The password to login with. If omitted, the password will be prompted for interactively.
    #[clap(long)]
    password: Option<String>,

    /// The action to perform
    #[command(subcommand)]
    action: AriadneXmppSubcommand,
}

#[derive(Subcommand)]
enum AriadneXmppSubcommand {
    /// Gets all ariadne proofs for a specific Jabber ID
    Get {
        /// The user to fetch ariadne profile for (optional, by default the --jid argument will be used)
        user: Option<Jid>,
    },
    /// Sets up the logged-in account to store ariadne proofs (creates the pubsub node)
    Setup,
    /// Un-sets up the logged-in account to store ariadne proofs (deletes the pubsub node)
    Unsetup,
    /// Adds a proof to the logged-in account's ariadne proofs
    AddProof {
        /// The proof to add
        proof: String,
    },
    /// Removes a proof from the logged=in account's ariadne proofs
    RemoveProof {
        // The proof to remove
        proof: String,
    },
}

#[tokio::main]
async fn main() {
    let args = AriadneXmppCommand::parse();

    let password = args.password.unwrap_or_else(|| {
        Password::with_theme(&ColorfulTheme::default())
            .with_prompt("Password:")
            .allow_empty_password(false)
            .interact()
            .expect("Unable to prompt on stderr")
    });

    // Client instance
    let mut client = Client::new_with_jid(args.jid.clone(), password)
        .await
        .unwrap();

    match args.action {
        AriadneXmppSubcommand::Get { user } => {
            let resp = xmpp_request(
                &mut client,
                Iq::from_get(
                    "ariadne-profiles-get",
                    PubSub::Items(Items::new("http://ariadne.id/protocol/proof")),
                )
                .with_to(user.unwrap_or(args.jid)),
                "ariadne-profiles-get",
            )
            .await
            .expect("Unable to request data from xmpp server");

            // Check for error
            if resp.attr("type").unwrap_or("") == "error" {
                panic!("Unable to fetch ariadne proofs from xmpp account. This account likely is not set up for ariadne proofs.")
            }

            let pubsub_child = resp
                .get_child("pubsub", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");
            let pubsub_items = pubsub_child
                .get_child("items", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");

            let mut found_one = false;
            for item in pubsub_items.children() {
                if item.name() != "item" && item.ns() != "http://jabber.org/protocol/pubsub" {
                    continue;
                } else if found_one != true {
                    println!("Ariadne proofs:");
                    found_one = true
                }

                let value = item
                    .get_child("value", "http://jabber.org/protocol/pubsub")
                    .expect("Invalid xmpp response");
                let value = value.texts().nth(0).expect("Invalid xmpp response");

                println!("- {value}");
            }

            if !found_one {
                println!("Xmpp account was set up for ariadne proofs, but none were added")
            }
        }
        AriadneXmppSubcommand::Setup => {
            let resp = xmpp_request(
                &mut client,
                Iq::from_set(
                    "ariadne-profiles-setup",
                    PubSub::Create {
                        create: Create {
                            node: Some(
                                NodeName::from_str("http://ariadne.id/protocol/proof").unwrap(),
                            ),
                        },
                        configure: Some(Configure {
                            form: Some(DataForm {
                                form_type: Some(
                                    "http://jabber.org/protocol/pubsub#node_config".to_owned(),
                                ),
                                title: Some("Ariadne Identity Proofs".to_owned()),
                                fields: vec![
                                    Field {
                                        var: "pubsub#max_items".to_owned(),
                                        label: None,
                                        media: vec![],
                                        options: vec![],
                                        required: false,
                                        values: vec!["64".to_owned()],
                                        type_: xmpp_parsers::data_forms::FieldType::Fixed,
                                    },
                                    Field {
                                        var: "pubsub#persist_items".to_owned(),
                                        label: None,
                                        media: vec![],
                                        options: vec![],
                                        required: false,
                                        values: vec!["true".to_owned()],
                                        type_: xmpp_parsers::data_forms::FieldType::Fixed,
                                    },
                                    Field {
                                        var: "pubsub#access_model".to_owned(),
                                        label: None,
                                        media: vec![],
                                        options: vec![],
                                        required: false,
                                        values: vec!["open".to_owned()],
                                        type_: xmpp_parsers::data_forms::FieldType::Fixed,
                                    },
                                ],
                                type_: xmpp_parsers::data_forms::DataFormType::Submit,
                                instructions: None,
                            }),
                        }),
                    },
                ),
                "ariadne-profiles-setup",
            )
            .await
            .expect("Invalid xmpp response");

            match resp.attr("type") {
                Some("result") => println!("Successfully setup account for ariadne proofs"),
                Some("error") => {
                    let error = resp
                        .get_child("error", "jabber:client")
                        .expect("Invalid xmpp response");
                    if error.has_child("conflict", "urn:ietf:params:xml:ns:xmpp-stanzas") {
                        eprintln!("This account is already set up for ariadne proofs.")
                    } else {
                        eprintln!("An unknown error occurred while trying to set up this account for ariadne proofs.")
                    }
                }
                _ => panic!("Invalid xmpp response"),
            }
        }
        AriadneXmppSubcommand::Unsetup => {
            let resp = xmpp_request(
                &mut client,
                Iq::from_set(
                    "ariadne-profiles-unsetup",
                    PubSubOwner::Delete(Delete {
                        node: NodeName::from_str("http://ariadne.id/protocol/proof").unwrap(),
                        redirect: None,
                    }),
                ),
                "ariadne-profiles-unsetup",
            )
            .await
            .expect("Invalid xmpp response");

            match resp.attr("type") {
                Some("result") => println!("Successfully un-setup account for ariadne proofs"),
                Some("error") => {
                    let error = resp
                        .get_child("error", "jabber:client")
                        .expect("Invalid xmpp response");
                    if error.has_child("item-not-found", "urn:ietf:params:xml:ns:xmpp-stanzas") {
                        eprintln!("This account is not setup for ariadne proofs.")
                    } else {
                        eprintln!("An unknown error occurred while trying to un-setup this account for ariadne proofs.")
                    }
                }
                _ => panic!("Invalid xmpp response"),
            }
        }
        AriadneXmppSubcommand::AddProof { proof } => {
            let query_resp = xmpp_request(
                &mut client,
                Iq::from_get(
                    "ariadne-profiles-querypubsub",
                    DiscoInfoQuery {
                        node: Some("http://ariadne.id/protocol/proof".to_owned()),
                    },
                ),
                "ariadne-profiles-querypubsub",
            )
            .await
            .expect("Invalid xmpp response");

            match query_resp.attr("type") {
                Some("result") => (),
                Some("error") => {
                    eprintln!("Account is not setup for ariadne proofs. Please run the `setup` command first.");
                    std::process::exit(1);
                }
                _ => panic!("Invalid xmpp response"),
            };

            // Check if the proof is already added
            let resp = xmpp_request(
                &mut client,
                Iq::from_get(
                    "ariadne-profiles-add-proof-get-existing",
                    PubSub::Items(Items::new("http://ariadne.id/protocol/proof")),
                )
                .with_to(args.jid),
                "ariadne-profiles-add-proof-get-existing",
            )
            .await
            .expect("Unable to request data from xmpp server");

            // Check for error
            if resp.attr("type").unwrap_or("") == "error" {
                panic!("Unable to fetch ariadne proofs from xmpp account, unknown error")
            }

            let pubsub_child = resp
                .get_child("pubsub", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");
            let pubsub_items = pubsub_child
                .get_child("items", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");

            for item in pubsub_items.children() {
                if item.name() != "item" && item.ns() != "http://jabber.org/protocol/pubsub" {
                    continue;
                }

                let value = item
                    .get_child("value", "http://jabber.org/protocol/pubsub")
                    .expect("Invalid xmpp response");
                let value = value.texts().nth(0).expect("Invalid xmpp response");

                if value == &proof {
                    eprintln!("The specified proof has already been added to this account.");
                    std::process::exit(1);
                }
            }

            let add_proof_resp = xmpp_request(
                &mut client,
                Iq::from_set(
                    "ariadne-profiles-addproof",
                    PubSub::Publish {
                        publish: Publish {
                            items: vec![Item(PubSubItem {
                                id: Some(ItemId(Uuid::new_v4().to_string())),
                                publisher: None,
                                payload: Some(
                                    Element::builder("value", "http://jabber.org/protocol/pubsub")
                                        .append(proof)
                                        .build(),
                                ),
                            })],
                            node: NodeName::from_str("http://ariadne.id/protocol/proof").unwrap(),
                        },
                        publish_options: Some(PublishOptions {
                            form: Some(DataForm {
                                form_type: Some(
                                    "http://jabber.org/protocol/pubsub#publish-options".to_owned(),
                                ),
                                title: None,
                                fields: vec![
                                    Field {
                                        var: "pubsub#persist_items".to_owned(),
                                        label: None,
                                        media: vec![],
                                        options: vec![],
                                        required: false,
                                        values: vec!["true".to_owned()],
                                        type_: xmpp_parsers::data_forms::FieldType::Fixed,
                                    },
                                    Field {
                                        var: "pubsub#access_model".to_owned(),
                                        label: None,
                                        media: vec![],
                                        options: vec![],
                                        required: false,
                                        values: vec!["open".to_owned()],
                                        type_: xmpp_parsers::data_forms::FieldType::Fixed,
                                    },
                                ],
                                type_: xmpp_parsers::data_forms::DataFormType::Submit,
                                instructions: None,
                            }),
                        }),
                    },
                ),
                "ariadne-profiles-addproof",
            )
            .await
            .expect("Invalid xmpp response");

            match add_proof_resp.attr("type") {
                Some("result") => println!("Successfully added proof to account."),
                Some("error") => {
                    eprintln!("Unable to add proof to account, an unknown error occurred.")
                }
                _ => panic!("Invalid xmpp response"),
            }
        }
        AriadneXmppSubcommand::RemoveProof { proof } => {
            let query_resp = xmpp_request(
                &mut client,
                Iq::from_get(
                    "ariadne-profiles-get",
                    PubSub::Items(Items::new("http://ariadne.id/protocol/proof")),
                )
                .with_to(args.jid),
                "ariadne-profiles-get",
            )
            .await
            .expect("Unable to request data from xmpp server");

            // Check for error
            if query_resp.attr("type").unwrap_or("") == "error" {
                panic!("Unable to fetch ariadne proofs from xmpp account. This account likely is not set up for ariadne proofs.")
            }

            let pubsub_child = query_resp
                .get_child("pubsub", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");
            let pubsub_items = pubsub_child
                .get_child("items", "http://jabber.org/protocol/pubsub")
                .expect("Invalid xmpp response");

            let mut values = Vec::<(&str, &Element)>::new();
            for item in pubsub_items.children() {
                if item.name() != "item" && item.ns() != "http://jabber.org/protocol/pubsub" {
                    continue;
                }

                let value = item
                    .get_child("value", "http://jabber.org/protocol/pubsub")
                    .expect("Invalid xmpp response");

                values.push((item.attr("id").expect("Invalid xmpp response"), value));
            }

            let Some((id, _)) = values.iter().find(|(_, value)| value.text() == proof) else {
                eprintln!("That proof was not added to this account.");
                std::process::exit(1);
            };

            let remove_resp = xmpp_request(
                &mut client,
                Iq::from_set(
                    "ariadne-profiles-delete",
                    PubSub::Retract(Retract {
                        items: vec![Item(PubSubItem {
                            id: Some(ItemId(id.to_string())),
                            payload: None,
                            publisher: None,
                        })],
                        node: NodeName::from_str("http://ariadne.id/protocol/proof").unwrap(),
                        notify: Notify::False,
                    }),
                ),
                "ariadne-profiles-delete",
            )
            .await
            .expect("Invalid xmpp response");

            match remove_resp.attr("type") {
                Some("result") => println!("Successfully removed proof from account."),
                Some("error") => {
                    eprintln!("Unable to remove proof from account, an unknown error occurred.")
                }
                _ => panic!("Invalid xmpp response"),
            }
        }
    }
}

async fn xmpp_request(
    client: &mut Client,
    data: impl Into<Element>,
    id: &str,
) -> anyhow::Result<Element> {
    client.send_stanza(data).await?;

    loop {
        let Some(recv) = client.next().await else {
            continue;
        };

        let Ok(resp) = recv else {
            continue;
        };

        if resp.attr("id").unwrap_or("") != id {
            continue;
        }

        return Ok(resp);
    }
}
