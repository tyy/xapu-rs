# xapu-rs

This is the **X**MPP **A**riadne **P**roof **U**tility (rust edition). It is my version of the [XAPU](https://codeberg.org/keyoxide/xapu) tool made for the [Keyoxide project](https://keyoxide.org). This is a command line program that allows you to fetch proofs for a Jabber ID, setup and unsetup a logged in account to store ariadne proofs, and modify the proofs stored on a logged-in account.

# Why

I made this because the other version is written in JavaScript and designed to run in a browser, which limits the amount of XMPP servers it can connect to (mainly ones that do not support [XEP-0156](https://xmpp.org/extensions/xep-0156.html) or alternative connection methods described in [XEP-0124](https://xmpp.org/extensions/xep-0124.html) and [XEP-0206](https://xmpp.org/extensions/xep-0206.html)). The XMPP server I wanted to add proofs for did not support the other tool, so I created this one which is specifically designed to run outside of the browser, and work with more XMPP servers.

## Usage

```
xapu-rs --jid <JID> --password <PASSWORD> <COMMAND>

Commands:
  get           Gets all ariadne proofs for a specific Jabber ID
  setup         Sets up the logged-in account to store ariadne proofs (creates the pubsub node)
  unsetup       Un-sets up the logged-in account to store ariadne proofs (deletes the pubsub node)
  add-proof     Adds a proof to the logged-in account's ariadne proofs
  remove-proof  Removes a proof from the logged=in account's ariadne proofs
  help          Print this message or the help of the given subcommand(s)

Options:
      --jid <JID>            The Jabber ID to login with
      --password <PASSWORD>  The password to login with
  -h, --help                 Print help
  -V, --version              Print version
```

## Examples

| Description                              | Command                                                                      |
| ---------------------------------------- | ---------------------------------------------------------------------------- |
| Fetch proofs for a specific Jabber ID    | `xapu-rs --jid user@example.com --password <PASSWORD> get user2@example.com` |
| Fetch proofs for your own account        | `xapu-rs --jid user@example.com --password <PASSWORD> get`                   |
| Setup your account for storing proofs    | `xapu-rs --jid user@example.com --password <PASSWORD> setup`                 |
| Un-setup your account for storing proofs | `xapu-rs --jid user@example.com --password <PASSWORD> unsetup`               |
| Add a proof to your account              | `xapu-rs --jid user@example.com --password <PASSWORD> add-proof <PROOF>`     |
| Remove a proof from your account         | `xapu-rs --jid user@example.com --password <PASSWORD> remove-proof <PROOF>`  |
